// Fill out your copyright notice in the Description page of Project Settings.

#include "Blocker.h"
#include "Components/StaticMeshComponent.h"
#include "Characters/RunCharacter.h"

// Sets default values
ABlocker::ABlocker()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	ObstacleMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ObstacleMesh"));
	RootComponent = ObstacleMesh;

	ObstacleMesh->OnComponentHit.AddDynamic(this, &ABlocker::OnComponentHit);

}

// Called when the game starts or when spawned
void ABlocker::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABlocker::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABlocker::OnComponentHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow,"Component Hit");

	if (OtherActor) {

		DestroyOtherActor(OtherActor);
	}


}


void ABlocker::DestroyOtherActor(AActor* OtherActor) {

	ARunCharacter* RunCharacter = Cast<ARunCharacter>(OtherActor);
	if (RunCharacter) {

		RunCharacter->Die();
	}
}

