// Fill out your copyright notice in the Description page of Project Settings.

#include "Item.h"
#include "Components/SceneComponent.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/RotatingMovementComponent.h"
#include "Characters/RunCharacter.h"
#include "Sound/SoundBase.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AItem::AItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	RootComponent = SceneComponent;

	ItemMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Item"));
	ItemMesh->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);

	RotatingMovement = CreateDefaultSubobject<URotatingMovementComponent>(TEXT("RotatingMovementComp"));

	ItemMesh->OnComponentBeginOverlap.AddDynamic(this, &AItem::OnItemOverlap);
	ItemMesh->bGenerateOverlapEvents = true;
	ItemMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
}

int32 AItem::GetItemValue()
{
	return value;
}

// Called when the game starts or when spawned
void AItem::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void AItem::OnItemOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (OtherActor) {

		ARunCharacter* RunCharacter = Cast<ARunCharacter>(OtherActor);
		if (RunCharacter) {

			RunCharacter->PickupItem(this);

			if (OverlapSound) {
				UGameplayStatics::SpawnSoundAtLocation(GetWorld(), OverlapSound, GetActorLocation());
			}
		}
	}
}