// Fill out your copyright notice in the Description page of Project Settings.

#include "RunCharacter.h"

#include "GameFramework/Controller.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystem.h"
#include "Sound/SoundBase.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "Items/Item.h"

// Sets default values
ARunCharacter::ARunCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	// set our turn rates for input
	

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;


	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	//CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

												// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

}

// Called when the game starts or when spawned
void ARunCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ARunCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//Continously moves forward

	if (!bIsDead) {
		MoveForward(1);
		TurnCorner(DeltaTime);
	}
}

// Called to bind functionality to input
void ARunCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction("TurnLeft", IE_Pressed, this,  &ARunCharacter::TurnLeft);
	//	PlayerInputComponent->BindAxis("TurnRate", this, &ABaseCharacter::TurnAtRate);
	PlayerInputComponent->BindAction("TurnRight", IE_Pressed, this, &ARunCharacter::TurnRight);
	PlayerInputComponent->BindAxis("MoveRight", this, &ARunCharacter::MoveRight);

}

void ARunCharacter::PickupItem(class AItem* item)
{
	int32 itemValue = item->GetItemValue();
	TotalCoins += itemValue;
	

	if (PickupSound) {
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), PickupSound, GetActorLocation());
	}
}

int32 ARunCharacter::GetTotalCoins()
{
	return TotalCoins;
}

void ARunCharacter::TurnLeft()
{

	if (bCanTurn) {

		DesiredRotation = FRotator(0, -90.f, 0);
		bCanTurn = false;
	}

}

void ARunCharacter::TurnRight() {

	if (bCanTurn) {

		 DesiredRotation = FRotator(0, 90.f, 0);
		 bCanTurn = false;
	}
}

//Does corner turn
void ARunCharacter::TurnCorner(float DeltaTime) {

	const FRotator Rotation = Controller->GetControlRotation();
	if (Rotation != DesiredRotation) {

		UWorld* World = GetWorld();
		check(World);

		FRotator NewRotation = FMath::RInterpTo(Rotation, DesiredRotation, DeltaTime, 10.0f); 
		Controller->SetControlRotation(NewRotation);
	}
}


void ARunCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void ARunCharacter::MoveRight(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void ARunCharacter::Die()
{
	bIsDead = true;
	
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Camera, ECollisionResponse::ECR_Ignore);
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	//GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetCapsuleComponent()->SetSimulatePhysics(false);

	USkeletalMeshComponent* Mesh = GetMesh();
	Mesh->SetCollisionResponseToChannel(ECC_PhysicsBody, ECollisionResponse::ECR_Ignore);
	Mesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	Mesh->SetAllBodiesBelowSimulatePhysics(Mesh->GetBoneName(1), true);
	Mesh->SetSimulatePhysics(true);

	GetCharacterMovement()->DisableMovement();
	//GetMovementComponent()->StopMovementImmediately();

	if (DeathParticle) {
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), DeathParticle, GetActorTransform());
	}

	if (DeathSound) {
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), DeathSound, GetActorLocation());
	}


	GetWorld()->GetTimerManager().SetTimer(RespawnHandler, this, &ARunCharacter::RestartLevel, 3.f, false);

	//GetMesh()->SetVisibility(false);
	//SetLifeSpan(3);
}


void ARunCharacter::RestartLevel() {

	UWorld* World = GetWorld();
	if (World) {

		UGameplayStatics::OpenLevel(this, FName(*GetWorld()->GetName()));
	}
}