// Fill out your copyright notice in the Description page of Project Settings.

#include "FloorTile.h"
#include "Components/SceneComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/BoxComponent.h"
#include "Characters/RunCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "EndlessRunnerGameModeBase.h"
#include "Obstacles/Blocker.h"
#include "Items/Item.h"
#include "Characters/RunCharacter.h"
// Sets default values
AFloorTile::AFloorTile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	RootComponent = SceneComponent;
	
	AttachPoint = CreateDefaultSubobject<UArrowComponent>(TEXT("AttachPoint"));
	AttachPoint->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);

	SpawnPoint = CreateDefaultSubobject<UArrowComponent>(TEXT("SpawnPoint"));
	SpawnPoint->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);

	SpawnPointLeft = CreateDefaultSubobject<UArrowComponent>(TEXT("SpawnPointLeft"));
	SpawnPointLeft->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);

	SpawnPointRight = CreateDefaultSubobject<UArrowComponent>(TEXT("SpawnPointRight"));
	SpawnPointRight->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
	
	FloorMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Floor"));
	FloorMesh->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);

	WallMeshOne = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WallOne"));
	WallMeshOne->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);

	WallMeshTwo = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WallTwo"));
	WallMeshTwo->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
	
	//Trigger must be attached to root component ( or possibly another component ) so that the trigger does not get 'lost' in-game movement.
	//If it's 'lost' then new tiles can't be created.
	//In-effect, all triggers will get 'grouped', which causes a bunch of overlaps at once when the player goes over one. Those overlaps causes many tiles to generate at once.
	//To test this negative effect, comment out the line for, 'EndTrigger->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);'
	//Another note, sometimes the editor has to be restarted for changes to take affect.
	EndTrigger = CreateDefaultSubobject<UBoxComponent>(TEXT("EndTriggerComponent"));
	EndTrigger->OnComponentBeginOverlap.AddDynamic(this, &AFloorTile::OnOverlap);
	EndTrigger->bGenerateOverlapEvents = true;
	EndTrigger->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	EndTrigger->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);


	CoinArea = CreateDefaultSubobject<UBoxComponent>(TEXT("CoinArea"));
	CoinArea->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);

}

// Called when the game starts or when spawned
void AFloorTile::BeginPlay()
{
	Super::BeginPlay();
	SetSpawnPoints();

	int32 RandValue = FMath::RandRange(0, 1);
	switch (RandValue) {

	case 0: 
		SpawnBlocker();
		break;
	case 1:
		SpawnCoins();
		break;
	default: 
		SpawnBlocker();
	}

}

// Called every frame
void AFloorTile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

FTransform AFloorTile::GetAttachTransform()
{
	return AttachPoint->GetComponentTransform();
}

void AFloorTile::OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (OtherActor) {

		ARunCharacter* RunCharacter = Cast<ARunCharacter>(OtherActor);
		if (RunCharacter) {
			DynamicallyAddTile(RunCharacter);
		}
	}
}

void AFloorTile::DynamicallyAddTile(AActor* OtherActor) {

	AGameModeBase* GameMode = UGameplayStatics::GetGameMode(GetWorld());
	AEndlessRunnerGameModeBase* EndlessRunnerGameMode = Cast<AEndlessRunnerGameModeBase>(GameMode);
	if (EndlessRunnerGameMode) {
		EndlessRunnerGameMode->AddFloorTile();
		GetWorld()->GetTimerManager().SetTimer(RemoveTileHandler, this, &AFloorTile::RemoveTile, 2.f, false);
	}
}

void AFloorTile::RemoveTile() {


	for (int32 i = 0; i < Blockers.Num(); i++) {
		Blockers[i]->Destroy();
	}

	for (int32 i = 0; i < Items.Num(); i++) {
		Items[i]->Destroy();
	}
	Destroy();
}

void AFloorTile::SetSpawnPoints()
{

	SpawnPoints.Add(SpawnPoint->GetComponentLocation());
	SpawnPoints.Add(SpawnPointLeft->GetComponentLocation());
	SpawnPoints.Add(SpawnPointRight->GetComponentLocation());

}

void AFloorTile::SpawnBlocker()
{

	//Instead of using ChildActor in Blueprint, we just spawn the actor, keep track of them, and destroy them when the FloorTile is destroyed (RemoveTile function)
	UWorld* World = GetWorld();
	if (World && SpawnPoints.Num() > 0) {

		int32 SpawnIndex = FMath::RandRange(0, SpawnPoints.Num() - 1);
		FVector SpawnLocation = SpawnPoints[SpawnIndex];

		ABlocker* SpawnedBlocker = World->SpawnActor<ABlocker>(BlockerClass, SpawnLocation, FRotator::ZeroRotator);


		if (SpawnedBlocker) {
			Blockers.Add(SpawnedBlocker);
		}
	}

}

void AFloorTile::SpawnCoins()
{
	//Instead of using ChildActor in Blueprint, we just spawn the actor, keep track of them, and destroy them when the FloorTile is destroyed (RemoveTile function)
	UWorld* World = GetWorld();
	if (World && SpawnPoints.Num() > 0) {


		//Spawn a random number of coins in random locations on the floor tile
		int32 NumCoins = FMath::RandRange(0, MaxCoins -1);
		for (int i = 0; i < NumCoins; i++) {
			FVector SpawnLocation = UKismetMathLibrary::RandomPointInBoundingBox(CoinArea->Bounds.Origin, CoinArea->Bounds.BoxExtent);
			AItem* SpawnedItem = World->SpawnActor<AItem>(ItemClass, SpawnLocation, FRotator::ZeroRotator);

			if (SpawnedItem) {
				Items.Add(SpawnedItem);
			}

		}
	}
}

