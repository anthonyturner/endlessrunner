// Fill out your copyright notice in the Description page of Project Settings.

#include "EndlessRunnerGameModeBase.h"
#include "UObject/ConstructorHelpers.h"
#include "Tiles/FloorTile.h"



AEndlessRunnerGameModeBase::AEndlessRunnerGameModeBase(){


	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/Characters/BP_RunCharacter"));

	//static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/Characters/TP_ThirdPersonCharacter"));
	DefaultPawnClass = PlayerPawnBPClass.Class;

	//static ConstructorHelpers::FClassFinder<APlayerController> PlayerPawnControllerBPClass(TEXT("/Game/Blueprints/Characters/HeroCharacter/BP_HeroController"));
	//PlayerControllerClass = PlayerPawnControllerBPClass.Class;


	//GameStateClass = AEndlessRunnerGameState::StaticClass();

}

// Called when the game starts or when spawned
void AEndlessRunnerGameModeBase::BeginPlay()
{
	Super::BeginPlay();


	for (int32 i = 0; i < 9; i++) {

		AddFloorTile();

	}

}

void AEndlessRunnerGameModeBase::AddFloorTile()
{
	UWorld* World = GetWorld();
//	check(World);
	// GetWorld()->SpawnActor<AProjectile>(Location, Rotation, SpawnInfo);

	if (World) {
		AFloorTile* SpawnedTile = World->SpawnActor<AFloorTile>(FloorTile, NextSpawnPoint.GetLocation(), FRotator::ZeroRotator);
		if (SpawnedTile) {

			NextSpawnPoint = SpawnedTile->GetAttachTransform();
		}
		else {

			GEngine->AddOnScreenDebugMessage(-1, 0.5f, FColor::Yellow, "Floor was not spawned");
		}
	}

}
