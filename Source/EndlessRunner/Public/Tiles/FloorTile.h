// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FloorTile.generated.h"

UCLASS()
class ENDLESSRUNNER_API AFloorTile : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	AFloorTile();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Floor")
		class USceneComponent* SceneComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Floor")
		class UStaticMeshComponent* FloorMesh;


	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Floor")
		class UStaticMeshComponent* WallMeshOne;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Floor")
		class UStaticMeshComponent* WallMeshTwo;


	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Floor")
		class UArrowComponent* AttachPoint;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Floor")
		class UArrowComponent* SpawnPoint;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Floor")
		class UArrowComponent* SpawnPointLeft;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Floor")
		class UArrowComponent* SpawnPointRight;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Floor")
		class UBoxComponent* EndTrigger;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Floor")
		class UBoxComponent* CoinArea;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Floor")
		TSubclassOf <class ABlocker> BlockerClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Items")
		TSubclassOf <class AItem> ItemClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Items")
		int32 MaxCoins = 3;	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	FTransform GetAttachTransform();

	UFUNCTION(BlueprintCallable)
	void DynamicallyAddTile(AActor* Actor);

	
	UFUNCTION()
		void RemoveTile();

	UFUNCTION(BlueprintCallable)
	void SetSpawnPoints();
	
	UFUNCTION(BlueprintCallable)
	void SpawnBlocker();

	UFUNCTION(BlueprintCallable)
		void SpawnCoins();

	UFUNCTION()
	void OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);
	
	
private:

	TArray<class AItem*>Items;
	TArray<class ABlocker*>Blockers;
	FTimerHandle RemoveTileHandler;
	TArray<FVector>SpawnPoints;

};
